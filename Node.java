import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;

/* 
 *  Program: Prosty edytor grafu
 *     Plik: Node.java
 *           
 *  Klasa Node implementuje klase reprezentującą wezeł  
 *            
 *    Autor: Adam  Cierniak
 *     Data:  listopad 2018 r.
 */

public class Node implements Serializable
{
		protected int x;
		protected int y;
		
		protected int r;
		protected String name;
		
		private Color color;
		
		
		public Node(int x, int y) {
			this.x = x;
			this.y = y;
			this.r = 10;
			this.color = Color.WHITE;
		}
		
		public Node(int x, int y,String name, Color col)
		{
			this.x = x;
			this.y = y;
			this.r = 10;
			this.color = col;
			this.name = name;
		}
		
		public int getX() {
			return x;
		}
		public void setX(int x) {
			this.x = x;
		}
		
		public int getY() {
			return y;
		}
		
		public void setY(int y) {
			this.y = y;
		}
		
		public int getR() {
			return r;
		}

		public void setR(int r) {
			this.r = r;
		}

		public Color getColor() {
			return color;
		}

		public void setColor(Color color) {
			this.color = color;
		}
		
		public boolean isMouseOver(int mx, int my){
			return (x-mx)*(x-mx)+(y-my)*(y-my)<=r*r;
		}
		
		public void setName(String name)
		{
			this.name= name;
		}
		
		public String getName()
		{
			return name;
		}

		void draw(Graphics g) 
		{
			Font  font = new Font("SansSerif", 1, 16);  //ustawienie czcionki 
			
			if(name == null)
			{
				
			}
			
		    g.setColor(this.color);
		    g.fillOval(this.x - r, this.y - r, 2*r, 2*r);
		    g.setColor(Color.BLACK);
		    g.drawOval(this.x - r, this.y - r, 2*r, 2*r);
		    g.setFont(font);
		    FontRenderContext frc = ((Graphics2D)g).getFontRenderContext(); //rzutowanie na Graphic2d
		    Rectangle2D bounds = font.getStringBounds(this.name, frc) ; 
		    g.drawString(this.name, this.x - (int)bounds.getWidth() / 2, this.y + (int)bounds.getHeight() / 4);
		}
		
		@Override
		public String toString(){
			return ("(" + x +", " + y + ", " + r + ")");
		}
}
