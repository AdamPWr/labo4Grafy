import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.Serializable;

/* 
 *  Program: Prosty edytor grafu
 *     Plik: Edge.java
 *           
 *  Klasa Edge klase krawedzi grafu.  
 *            
 *    Autor: Adam  Cierniak
 *     Data:  listopad 2018 r.
 */

public class Edge implements Serializable
{
	  private static final long serialVersionUID = 1L;
	  protected Node nodeFrom;
	  protected Node nodeTo;
	  Color color;
	  
	  public Edge(Node nodeFrom, Node nodeTo)
	  {
		this.nodeFrom = nodeFrom;
		this.nodeTo = nodeTo;
	    this.color = Color.RED;
	  }
	  
	  public Edge(Node nodeFrom, Node nodeTo, Color col)
	  {
		this.nodeFrom = nodeFrom;
		this.nodeTo = nodeTo;
	    this.color = col;
	  }
	  
	  public Color getColor()
	  {
	    return this.color;
	  }
	  
	  public void setColor(Color color)
	  {
	    this.color = color;
	  }
	  
	  public Node getNodeFrom()
	  {
	    return this.nodeFrom;
	  }
	  
	  public Node getNodeTo()
	  {
	    return this.nodeTo;
	  }
	  
	  public boolean containsNode(Node node)
	  {
		  if(nodeFrom.equals(node) || nodeTo.equals(node))return true;
		  else return false;
	  }
	  public boolean checkMousePosition(int WspX, int WspY)
	  {
		  	if (((WspY > this.nodeFrom.getY()) && (WspY > this.nodeTo.getY())) || (
				  (WspY < this.nodeFrom.getY()) && (WspY < this.nodeTo.getY()))) {
		  		return false;
		  	}
		    if (((WspX > this.nodeFrom.getX()) && (WspX > this.nodeTo.getX())) || (
		      (WspX < this.nodeFrom.getX()) && (WspX < this.nodeTo.getX()))) {
		      return false;
		    }
		    
		    // rownanie na odleglosc punktu od prostej 
		    double niedokladnosc = 5;  // o tyle mozebyc oddalony
		    int A = this.nodeTo.getY() - this.nodeFrom.getY();//wspA
		    int B = this.nodeFrom.getX() - this.nodeTo.getX();//WspB
		    int C = this.nodeTo.getX() * this.nodeFrom.getY() - this.nodeFrom.getX() * this.nodeTo.getY(); //wspC
		    double d = Math.abs(A * WspX + B * WspY + C) / Math.sqrt(A * A + B * B);
		    if (d <= niedokladnosc) return  true ;
		    else return false;
	  }
	  
	  public String toString()
	  {
	    return "[" + this.nodeFrom + "==>" + this.nodeTo + "]";
	  }
	  
	  void draw(Graphics g)
	  {
	    g.setColor(this.color);
	    
	    ((Graphics2D)g).setStroke(new BasicStroke(2.0F));
	    g.drawLine(this.nodeFrom.getX(), this.nodeFrom.getY(), 
	      this.nodeTo.getX(), this.nodeTo.getY());
	    
	    ((Graphics2D)g).setStroke(new BasicStroke(1.0F));
	    
	    g.setColor(Color.BLACK);
	  }
}
