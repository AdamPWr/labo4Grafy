import java.awt.Graphics;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* 
 *  Program: Prosty edytor grafu
 *     Plik: Graph.java
 *           
 *  Klasa Graph implementuje klase składającą sie z wezlow i krawedzi 
 *            
 *    Autor: Adam  Cierniak
 *     Data:  listopad 2018 r.
 */
public class Graph  implements Serializable
{
	private static final long serialVersionUID = 1L;

	private List<Node> nodes;
	private List<Edge> edges;
	
	public Graph() {
		this.nodes = new ArrayList<Node>();	
		this.edges = new ArrayList<Edge>();
	}
	
	public void addNode(Node node){
		nodes.add(node);
	}
	
	public void removeNode(Node node){
		nodes.remove(node);
	}
	
	public Node[] getNodes(){
		Node [] array = new Node[0];
		return nodes.toArray(array);
	}
	
	public void addEdge(Edge edge)
	  {
		if (edge != null)
	    this.edges.add(edge);
	  }
	  
	  public void removeEdge(Edge edge)
	  {
	    this.edges.remove(edge);
	  }
	  
	  public Edge[] getEdges()
	  {
	    Edge[] array = new Edge[0];
	    return (Edge[])this.edges.toArray(array);
	  }
	  
	  
	  public void draw(Graphics g)
	  {
	    for (Edge e : this.edges) {
	      e.draw(g);
	    }
	    for (Node n : this.nodes) {
	      n.draw(g);
	    }
	  }
}
