import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;



/* 
 *  Program: Prosty edytor grafu
 *     Plik: GraphEditor.java
 *           
 *  Klasa GraphEditor implementuje okno glowne
 *  dla prostego graficznego edytora grafu.  
 *            
 *    Autor: Adam  Cierniak
 *     Data:  listopad 2018 r.
 */
public class GraphEditor extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;

	private static final String APP_TITLE = "Edytor grafow";

	private static final String APP_AUTHOR = "Adam Cierniak \n" +
			"Nr:241310";
	
	private static final String APP_INSTRUCTION = 		
			"                  O P I S   P R O G R A M U \n\n" + 
	        "Aktywna klawisze:\n" +
			"   strzalki ==> przesuwanie wszystkich k�\n" +
			"   SHIFT + strzalki ==> szybkie przesuwanie wszystkich k�\n\n" +
			"ponadto gdy kursor wskazuje kolo:\n" +
			"   DEL   ==> kasowanie kola\n" +
			"   +, -   ==> powiekszanie, pomniejszanie kola\n" +
			"   r,g,b ==> zmiana koloru ko�a\n\n" +
			"Operacje myszka:\n" +
			"   przeciaganie ==> przesuwanie wszystkich k�\n" +
			"   PPM ==> tworzenie nowego kola w niejscu kursora\n" +
	        "ponadto gdy kursor wskazuje kolo:\n" +
	        "   przeciaganie ==> przesuwanie kola\n" +
			"   PPM ==> zmiana koloru kola\n" +
	        "                   lub usuwanie kola\n";
	
	public static void main(String[] args) {
		new GraphEditor();
	}
	
	private JMenuBar menuBar 			= new JMenuBar();
	private JMenu menuGraph 			= new JMenu("Graph");
	private JMenu menuHelp 				= new JMenu("Help");
	private JMenu menuFile 				= new JMenu("File");
	private JMenuItem menuNew 			= new JMenuItem("New", KeyEvent.VK_N);
	private JMenuItem menuExample		= new JMenuItem("Example", KeyEvent.VK_X);
	private JMenuItem menuExit 			= new JMenuItem("Exit", KeyEvent.VK_E);
	private JMenuItem menuListOfNodes 	= new JMenuItem("List of Nodes", KeyEvent.VK_N);
	private JMenuItem menuListOfEdges 	= new JMenuItem("List of Edges", KeyEvent.VK_E);
	private JMenuItem menuAuthor 		= new JMenuItem("Author", KeyEvent.VK_A);
	private JMenuItem menuInstruction 	= new JMenuItem("Instruction", KeyEvent.VK_I);
	private JMenuItem menuSave 			= new JMenuItem("Save", KeyEvent.VK_S);
	private JMenuItem menuOpen 			= new JMenuItem("Open", KeyEvent.VK_O);
	
	private GraphPanel panel = new GraphPanel();
	
	
	public GraphEditor()
	{
		super(APP_TITLE);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(400,400);
		setLocationRelativeTo(null);
		setContentPane(panel);
		createMenu();
		showBuildinExample();
		setVisible(true);
	}
		
	private void createMenu() 
	{
		menuNew.addActionListener(this);
		menuExample.addActionListener(this);
		menuExit.addActionListener(this);
		menuListOfNodes.addActionListener(this);
		menuListOfEdges.addActionListener(this);
		menuAuthor.addActionListener(this);
		menuInstruction.addActionListener(this);
		menuSave.addActionListener(this);
		menuOpen.addActionListener(this);
		
		menuGraph.setMnemonic(KeyEvent.VK_G);
		menuFile.add(menuNew);
		menuFile.add(menuSave);
		menuFile.add(menuOpen);
		menuFile.addSeparator();
		menuGraph.add(menuListOfNodes);
		menuGraph.add(menuListOfEdges);
		menuFile.add(menuExample);
		menuFile.addSeparator();
		menuFile.add(menuExit);


		
		menuHelp.setMnemonic(KeyEvent.VK_H);
		menuHelp.add(menuInstruction);
		menuHelp.add(menuAuthor);
		
		menuBar.add(menuFile);
		menuBar.add(menuGraph);
		menuBar.add(menuHelp);
		setJMenuBar(menuBar);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Object source = e.getSource();
		
		
		if (source == menuNew) {
			panel.setGraph(new Graph());
		}
		if (source == menuExample) {
			showBuildinExample();
		}
		if (source == menuListOfNodes) {
			showListOfNodes(panel.getGraph());
		}
		if (source == menuListOfEdges) {
			showListOfEdges(panel.getGraph());
		}
		if (source == menuAuthor)
		{
			JOptionPane.showMessageDialog(this, APP_AUTHOR, APP_TITLE, JOptionPane.INFORMATION_MESSAGE);
		}
		if (source == menuInstruction) {
			JOptionPane.showMessageDialog(this, APP_INSTRUCTION, APP_TITLE, JOptionPane.PLAIN_MESSAGE);	
		}
		if(source == menuSave) {
			JFileChooser chooser = new JFileChooser(".");
			int returnVal = chooser.showSaveDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
			try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(chooser.getSelectedFile().getPath()))) {
			    outputStream.writeObject(panel.getGraph());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			}
		}
		
		if(source == menuOpen) {
			JFileChooser chooser = new JFileChooser(".");
			int returnVal = chooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(chooser.getSelectedFile().getPath()))) {
				    Graph graph = (Graph) inputStream.readObject();
				    panel.setGraph(graph);
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

}
		if (source == menuExit) 
		{
			System.exit(0);
		}
		
	}
	
	private void showBuildinExample() 
	{
	    Graph graph = new Graph();
	    
	    Node n1 = new Node(345, 34, "A", Color.CYAN);
	    Node n2 = new Node(244, 55, "B", Color.GREEN);
	    Node n3 = new Node(100, 300, "C", Color.RED);
	    Node n4 = new Node(286, 70, "C", Color.BLUE);
	    
	    graph.addNode(n1);
	    graph.addNode(n2);
	    graph.addNode(n3);
	    graph.addNode(n4);
	    
	    Edge e1 = new Edge(n1, n2, Color.YELLOW);
	    graph.addEdge(new Edge(n1, n2, Color.YELLOW));
	    graph.addEdge(new Edge(n2, n3, Color.BLUE));
	    graph.addEdge(new Edge(n1, n3, Color.CYAN));
	    graph.addEdge(e1);
	    

	    
	    this.panel.setGraph(graph);
	}

	private void showListOfNodes(Graph graph) {
		
		Node array[] = graph.getNodes();
		int i = 0;
		StringBuilder message = new StringBuilder("Liczba wezlow: " + array.length + "\n");
		for (Node node : array) {
			message.append(node + "    ");
			if (++i % 5 == 0)
				message.append("\n");
		}
		JOptionPane.showMessageDialog(this, message, APP_TITLE + " - Lista wezlow", JOptionPane.PLAIN_MESSAGE);
	}
	
	private void showListOfEdges(Graph graph) {
		
		Edge array[] = graph.getEdges();
		int i = 0;
		StringBuilder message = new StringBuilder("Liczba krawedi: " + array.length + "\n");
		for (Edge edge : array) {
			message.append(edge + "    ");
			if (++i % 5 == 0)
				message.append("\n");
		}
		JOptionPane.showMessageDialog(this, message, APP_TITLE + " - Lista wezlow", JOptionPane.PLAIN_MESSAGE);
	}
}
