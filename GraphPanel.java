import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JColorChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

/* 
 *  Program: Prosty edytor grafu
 *     Plik: GraphPanel.java
 *           
 *  Klasa GraphPanel implementuje panel na ktorym wyswietlony jest graf
 *  i umozliwia operacje na alementach grafu 
 *            
 *    Autor: Adam  Cierniak
 *     Data:  listopad 2018 r.
 */

public class GraphPanel extends JPanel
implements MouseListener, MouseMotionListener, KeyListener
{
	
	private static final long serialVersionUID = 1L;

	protected Graph graph;
	

	private int mouseX = 0;
	private int mouseY = 0;
	private boolean mouseButtonLeft = false;
	@SuppressWarnings("unused")
	private boolean mouseButtonRigth = false;
	protected int mouseCursor = Cursor.DEFAULT_CURSOR;
	
	protected Node nodeUnderCursor = null;
	protected Edge edgeUnderCursor = null;
	protected Node newEdgeNode		=null;

	public GraphPanel() {
		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
	    setFocusable(true);
	    requestFocus();
	}
	
	public Graph getGraph() {
		return graph;
	}
	
	public void setGraph(Graph graph) {
		this.graph = graph;
		repaint();
	}
	
	
	private Node findNode(int mx, int my){
		for(Node node: graph.getNodes()){
			if (node.isMouseOver(mx, my)){
				return node;
			}
		}
		return null;
	}
	
	private Node findNode(MouseEvent event){
		return findNode(event.getX(), event.getY());
	}
	
	private Edge findEdge(int mx, int my)
	{
		for(Edge edge: graph.getEdges()){
			if (edge.checkMousePosition(mx, my)){
				return edge;
			}
		}
		return null;
	}
	
	private Edge findEdge(MouseEvent event)
	{
		return findEdge(event.getX(), event.getY());
	}
	
	protected void setMouseCursor(MouseEvent event) {
		nodeUnderCursor = findNode(event);
		edgeUnderCursor = findEdge(event); // TUTAJ SKONCZYLEM
		if (nodeUnderCursor != null) {
			mouseCursor = Cursor.HAND_CURSOR;
		}
		else if(edgeUnderCursor!= null){
			mouseCursor = Cursor.CROSSHAIR_CURSOR;
		}
		else if(newEdgeNode != null)
		{
			mouseCursor = Cursor.WAIT_CURSOR;
		}
		else if (mouseButtonLeft) {
			mouseCursor = Cursor.MOVE_CURSOR;
		} else {
			mouseCursor = Cursor.DEFAULT_CURSOR;
		}
		setCursor(Cursor.getPredefinedCursor(mouseCursor));
		mouseX = event.getX();
		mouseY = event.getY();
	}
	
	protected void setMouseCursor() {
		nodeUnderCursor = findNode(mouseX, mouseY);
		if (nodeUnderCursor != null) {
			mouseCursor = Cursor.HAND_CURSOR;
		} else if (mouseButtonLeft) {
			mouseCursor = Cursor.MOVE_CURSOR;
		} else {
			mouseCursor = Cursor.DEFAULT_CURSOR;
		}
		setCursor(Cursor.getPredefinedCursor(mouseCursor));
	}
	
	private void moveNode(int dx, int dy, Node node){
		node.setX(node.getX()+dx);
		node.setY(node.getY()+dy);
	}
	
	private void moveAllNodes(int dx, int dy) {
		for (Node node : graph.getNodes()) {
			moveNode(dx, dy, node);
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (graph==null) return;
		graph.draw(g);
	}
	
	
	@Override
	public void keyTyped(KeyEvent e) {
		char znak=e.getKeyChar(); 
		if (nodeUnderCursor!=null){
		switch (znak) {
		case 'r':
			nodeUnderCursor.setColor(Color.RED);
			break;
		case 'g':
			nodeUnderCursor.setColor(Color.GREEN);
			break;
		case 'b':
			nodeUnderCursor.setColor(Color.BLUE);
			break;
		case '+':
			int r = nodeUnderCursor.getR()+10;
			nodeUnderCursor.setR(r);
			break;
		case '-':
			r = nodeUnderCursor.getR()-10;
			if (r>=10) nodeUnderCursor.setR(r);
			break;
		}
		repaint();
		setMouseCursor();
		}
		
	}

	@Override
	public void keyPressed(KeyEvent e) 
	{
		{  int dist;
	       if (e.isShiftDown()) dist = 10;
	                         else dist = 1;
			switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				moveAllNodes(-dist, 0);
				break;
			case KeyEvent.VK_RIGHT:
				moveAllNodes(dist, 0);
				break;
			case KeyEvent.VK_UP:
				moveAllNodes(0, -dist);
				break;
			case KeyEvent.VK_DOWN:
				moveAllNodes(0, dist);
				break;
			case KeyEvent.VK_DELETE:
				if (nodeUnderCursor != null) {
					graph.removeNode(nodeUnderCursor);
					nodeUnderCursor = null;
				}
				break;
			}
		}
		repaint();
		setMouseCursor();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) 
	{
		if (mouseButtonLeft) {
			if (nodeUnderCursor != null) {
				moveNode(e.getX() - mouseX, e.getY() - mouseY, nodeUnderCursor);
			} else {
				moveAllNodes(e.getX() - mouseX, e.getY() - mouseY);
			}
		}
		mouseX = e.getX();
		mouseY = e.getY();
		repaint();
		
	}

	@Override
	public void mouseMoved(MouseEvent e) 
	{
		setMouseCursor(e);	
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		if (e.getButton()==1) mouseButtonLeft = true;
		if (e.getButton()==3) mouseButtonRigth = true;
		setMouseCursor(e);
		
	}
	

	@Override
	public void mouseReleased(MouseEvent e) 
	{
		if (e.getButton() == 1)
			mouseButtonLeft = false;
		if(e.getButton() == 1 && newEdgeNode != null) {
			graph.addEdge(new Edge(findNode(e), newEdgeNode));
			repaint();
			newEdgeNode = null;
}
		if (e.getButton() == 3)
			mouseButtonRigth = false;
		
		setMouseCursor(e);
		if (e.getButton() == 3) 
		{
			if (nodeUnderCursor != null) {
				createPopupMenu(e, nodeUnderCursor);
			}
			else if(edgeUnderCursor != null){
				createPopupMenu(e, edgeUnderCursor);
			}
			else {
				createPopupMenu(e);
			}
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	protected void createPopupMenu(MouseEvent event) {
        JMenuItem menuItem;
 
        JPopupMenu popup = new JPopupMenu();
        menuItem = new JMenuItem("Create new node");
        
		menuItem.addActionListener((action) -> {
			graph.addNode(new Node(event.getX(), event.getY(),"?",Color.WHITE));
			repaint();
		});

		popup.add(menuItem);
        popup.show(event.getComponent(), event.getX(), event.getY());
    }
	

	protected void createPopupMenu(MouseEvent event, Node node) 
	{
		JMenuItem menuItem;

		JPopupMenu popup = new JPopupMenu();
		menuItem = new JMenuItem("Change node color");
		
		menuItem.addActionListener((a) -> {
			Color newColor = JColorChooser.showDialog(
                    this,
                    "Choose Background Color",
                    node.getColor());
			if (newColor!=null){
				node.setColor(newColor);
			}
			repaint();
		});
		

		popup.add(menuItem);
		
		menuItem = new JMenuItem("Remove this node");	
		menuItem.addActionListener((action) -> {
			for(Edge edge : graph.getEdges())
			{
				if(edge.containsNode(node))
					graph.removeEdge(edge);
			}
			graph.removeNode(node);
			repaint();
		});
		
		popup.add(menuItem);
		
		menuItem = new JMenuItem("Change node name");	
		menuItem.addActionListener((action) -> {
			String newName = "";
			while(newName.isEmpty())
			{
				newName = (String) JOptionPane.showInputDialog("Enter node's name (one letter)");
				if(newName.length()==1)break;
			}
			node.setName(newName);
			repaint();
		});
		popup.add(menuItem);
		
		menuItem = new JMenuItem("Add edge ");	
		menuItem.addActionListener((action) -> {
			newEdgeNode = node;
		});
		
		popup.add(menuItem);
		popup.show(event.getComponent(), event.getX(), event.getY());
	}
	
	protected void createPopupMenu(MouseEvent event, Edge edge) 
	{
		JMenuItem menuItem;

		JPopupMenu popup = new JPopupMenu();
		menuItem = new JMenuItem("Remove edge");
		menuItem.addActionListener((a) -> {
			graph.removeEdge(edge);
			repaint();
		});
		
		popup.add(menuItem);
		
		menuItem = new JMenuItem("Change edge's  color");
		menuItem.addActionListener((a) -> {
			Color newColor = JColorChooser.showDialog(
                    this,
                    "Choose Background Color",
                    edge.getColor());
			if (newColor!=null){
				edge.setColor(newColor);
			}
			repaint();
		});
		popup.add(menuItem);
		popup.show(event.getComponent(), event.getX(), event.getY());
	}
}
